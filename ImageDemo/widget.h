﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QMouseEvent>
#include <QLabel>

typedef struct SelectPic
{
    QLabel *Label;
    int X;
    int Y;
}SelectPic;

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void mousePressEvent(QMouseEvent * e);

private:
    Ui::Widget *ui;

    QVector<SelectPic> LabelList;

    void Init();
};

#endif // WIDGET_H
