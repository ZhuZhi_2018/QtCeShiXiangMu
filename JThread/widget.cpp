﻿#include "widget.h"
#include "ui_widget.h"
#include <QDebug>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    Td1 = new Thread1;
    Td1->moveToThread(&Thread);
    connect(&Thread, &QThread::finished, Td1, &QObject::deleteLater);
    connect(this, SIGNAL(StartThread()), Td1, SLOT(doWork()));
    Thread.start();
}

Widget::~Widget()
{
    delete ui;
    delete Td1;
    Thread.quit();
    Thread.wait();
}

void Widget::on_pushButton_clicked()
{
    emit StartThread();
}

void Widget::on_pushButton_2_clicked()
{
    Td1->Stop();
    qDebug() << "thread end.";
}
