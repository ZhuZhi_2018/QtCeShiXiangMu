﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMenu>

/**
  功能说明：
  1. 添加文件
  2. 添加过滤类型筛选文件
  3. 过滤子目录下文件
  4. 打开文件所在位置
  5. 删除/清除文件
  6. 删除所选文件类型所有文件
  7. 开始统计计算文件相关信息
 */

/**
 * 语言注释方式：
 * C / C++ / Java / C#:
 * // 注释内容
 * \/\* 注释内容 */

/** PHP
 * # 注释内容
 * // 注释内容
 * \/\* 注释内容 */

/** HTML / XML
 * <!---->
 */

/** Python
 * # 注释内容
 * ''' 注释内容，可多行注释  '''
 * """ 注释内容，可多行注释 """
 */


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_btnDir_clicked();

    void on_btnFile_clicked();

    void on_btnStart_clicked();

    void on_btnStop_clicked();

    void on_tableWidget_customContextMenuRequested(const QPoint &pos);

    void on_actionDelete_triggered();

    void on_actionClean_triggered();

    void on_actionDeleteType_triggered();

    void on_actionFilePos_triggered();

    void on_actionCommentHelp_triggered();

    void on_actionPython_triggered();

    void on_actionC_triggered();

    void on_actionCXX_triggered();

    void on_actionR_triggered();

    void on_actionMySQL_triggered();

    void on_actionSQL_Server_triggered();

    void on_actionOracle_PLSQL_triggered();

    void on_actionMatlab_triggered();

private:
    void setTableData(int row, const QString &name, const QString &type, int size);

    QStringList getFiles(const QString &dir, const QStringList &filter);

    void parseFileInfo(const QStringList &files);

private:
    Ui::MainWindow *ui;

    QStringList m_files;
    bool        m_stop;

    int         m_codeLines;
    int         m_commentLines;
    int         m_spaceLines;
    int         m_sumLines;
    int         m_fileSum;
    double      m_fileSize;
    int         m_fileTypeSum;

    bool        m_c;                // C
    bool        m_cxx;              // C++
    bool        m_python;           // python
    bool        m_r;                // R
    bool        m_mysql;            // MySQL
    bool        m_sqlServer;        // SQL Server
    bool        m_oracle;           // Oracle
    bool        m_matlab;           // Matlab

    // UI
    QMenu       m_rightMenu;
};
#endif // MAINWINDOW_H
