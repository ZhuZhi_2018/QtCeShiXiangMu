﻿#ifndef JLOGGER_H
#define JLOGGER_H

/**
  源码引用自“飞扬青云”
  在源码的基础上进行了修改
*/

#include <QObject>

class QFile;
class QTcpSocket;
class QTcpServer;

#ifdef quc
#if (QT_VERSION < QT_VERSION_CHECK(5,7,0))
#include <QtDesigner/QDesignerExportWidget>
#else
#include <QtUiPlugin/QDesignerExportWidget>
#endif
class QDESIGNER_WIDGET_EXPORT JLogger : public QObject
#else
class JLogger : public QObject
#endif
{
    Q_OBJECT
public:
    static JLogger *Instance();
    explicit JLogger(QObject *parent = nullptr);
    ~JLogger();

signals:
    void send(const QString &content);

public slots:
    //启动日志服务
    void start();
    //暂停日志服务
    void stop();
    //保存日志
    void save(const QString &content, QtMsgType type = QtInfoMsg);

    //设置是否重定向到网络
    void setToNet(bool toNet);
    //设置日志文件存放路径
    void setPath(const QString &path);
    //设置日志文件名称
    void setName(const QString &name);

private:
    static QScopedPointer<JLogger> self;

    //文件对象
    QFile *file;
    //是否重定向到网络
    bool toNet;
    //日志文件路径
    QString path;
    //日志文件名称
    QString name;
    //日志文件完整名称
    QString fileName;
};


class SendLog : public QObject
{
    Q_OBJECT
public:
    static SendLog *Instance();
    explicit SendLog(QObject *parent = nullptr);
    ~SendLog();

private:
    static QScopedPointer<SendLog> self;
    QTcpSocket *socket;
    QTcpServer *server;

private slots:
    void newConnection();

public slots:
    //发送日志
    void send(const QString &content);
};

#endif // JLOGGER_H
