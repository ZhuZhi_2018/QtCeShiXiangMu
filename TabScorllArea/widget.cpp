﻿#include "widget.h"
#include "ui_widget.h"
#include <QDebug>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
    , btnTest1(new QPushButton(QString::fromLocal8Bit("账号")))
    , btnTest2(new QPushButton(QString::fromLocal8Bit("常规")))
    , btnTest3(new QPushButton(QString::fromLocal8Bit("播放")))
    , btnTest4(new QPushButton(QString::fromLocal8Bit("消息与隐私")))
    , btnTest5(new QPushButton(QString::fromLocal8Bit("快捷键")))
    , btnTest6(new QPushButton(QString::fromLocal8Bit("下载设置")))
    , btnTest7(new QPushButton(QString::fromLocal8Bit("歌词")))
    , btnTest8(new QPushButton(QString::fromLocal8Bit("工具")))
    , vLayOperation(new QVBoxLayout)
    , sepLine(new QFrame)
    , labelAccount(new QLabel(QString::fromLocal8Bit("账号")))
    , btnweibo(new QPushButton(QString::fromLocal8Bit("微博")))
    , btnweichat(new QPushButton(QString::fromLocal8Bit("微信")))
    , btnQQ(new QPushButton(QString::fromLocal8Bit("QQ")))
    , btnDouban(new QPushButton(QString::fromLocal8Bit("豆瓣")))
    , btnYixin(new QPushButton(QString::fromLocal8Bit("易信")))
    , btnBound(new QToolButton)
    , hLayBound(new QHBoxLayout)
    , btnPerInfo(new QToolButton)
    , btnFriend(new QToolButton)
    , hLayPerson(new QHBoxLayout)
    , vLayAccount(new QVBoxLayout)
    , lineAccount(new QFrame)
    , labelGeneral(new QLabel(QString::fromLocal8Bit("常规")))
    , labelFont(new QLabel(QString::fromLocal8Bit("字体选择")))
    , cBoxFont(new QComboBox)
    , labelStart(new QLabel(QString::fromLocal8Bit("启动")))
    , checkStart(new QCheckBox(QString::fromLocal8Bit("开机启动")))
    , labelConnect(new QLabel(QString::fromLocal8Bit("关联")))
    , checkConnect(new QCheckBox(QString::fromLocal8Bit("将网易云设为默认播放器")))
    , labelAnimate(new QLabel(QString::fromLocal8Bit("动画")))
    , checkAnimate(new QCheckBox(QString::fromLocal8Bit("禁用动画效果")))
    , labelGPU(new QLabel(QString::fromLocal8Bit("GPU 加速")))
    , checkGPU(new QCheckBox(QString::fromLocal8Bit("禁用 GPU 加速")))
    , labelColse(new QLabel(QString::fromLocal8Bit("关闭主面板")))
    , rbtnMin(new QRadioButton(QString::fromLocal8Bit("最小化到系统托盘")))
    , rbtnClose(new QRadioButton(QString::fromLocal8Bit("退出云音乐")))
    , labelTimedColse(new QLabel(QString::fromLocal8Bit("定时关机")))
    , checkTimedClose(new QCheckBox(QString::fromLocal8Bit("开启定时关机")))
    , vLayGeneral(new QVBoxLayout)
    , lineGeneral(new QFrame)
    , labelPlay(new QLabel(QString::fromLocal8Bit("播放")))
    , labelPlayList(new QLabel(QString::fromLocal8Bit("播放列表")))
    , rbtnReplaceList(new QRadioButton(QString::fromLocal8Bit("替换")))
    , rbtnAddList(new QRadioButton(QString::fromLocal8Bit("添加")))
    , labelAutoPlay(new QLabel(QString::fromLocal8Bit("自动播放")))
    , checkAutoPlay(new QCheckBox(QString::fromLocal8Bit("自动播放")))
    , labelProgress(new QLabel(QString::fromLocal8Bit("播放进度")))
    , checkProgress(new QCheckBox(QString::fromLocal8Bit("播放进度")))
    , labelEffect(new QLabel(QString::fromLocal8Bit("效果")))
    , checkEffect(new QCheckBox(QString::fromLocal8Bit("开启音乐淡入淡出")))
    , labelDevice(new QLabel(QString::fromLocal8Bit("输出设备")))
    , cBoxDevice(new QComboBox)
    , vLayPlay(new QVBoxLayout)
    , linePlay(new QFrame)
    , labelMessage(new QLabel(QString::fromLocal8Bit("消息与隐私")))
    , labelPrivacyMsg(new QLabel(QString::fromLocal8Bit("私信")))
    , rbtnEveryone(new QRadioButton(QString::fromLocal8Bit("所有人")))
    , rbtnAttention(new QRadioButton(QString::fromLocal8Bit("我所关注的人")))
    , labelNotify(new QLabel(QString::fromLocal8Bit("通知")))
    , checkSongList(new QCheckBox(QString::fromLocal8Bit("歌单被收藏")))
    , checkFavour(new QCheckBox(QString::fromLocal8Bit("收到赞")))
    , checkNew(new QCheckBox(QString::fromLocal8Bit("新粉丝")))
    , labelState(new QLabel(QString::fromLocal8Bit("推荐动态")))
    , checkState(new QCheckBox(QString::fromLocal8Bit("显示推荐动态")))
    , labelRank(new QLabel(QString::fromLocal8Bit("我的听歌排行")))
    , rbtnEveryoneSee(new QRadioButton(QString::fromLocal8Bit("所有人可见")))
    , rbtnAttentionSee(new QRadioButton(QString::fromLocal8Bit("我关注的人可见")))
    , rbtnOnlySelfSee(new QRadioButton(QString::fromLocal8Bit("仅自己可见")))
    , vLayMessage(new QVBoxLayout)
    , vLayScrollArea(new QVBoxLayout)
    , widgetMain(new QWidget)
    , scrollArea(new QScrollArea)
    , hLayMain(new QHBoxLayout)
{
    ui->setupUi(this);

    vLayOperation->addWidget(btnTest1);
    vLayOperation->addWidget(btnTest2);
    vLayOperation->addWidget(btnTest3);
    vLayOperation->addWidget(btnTest4);
    vLayOperation->addWidget(btnTest5);
    vLayOperation->addWidget(btnTest6);
    vLayOperation->addWidget(btnTest7);
    vLayOperation->addWidget(btnTest8);
    vLayOperation->addStretch();
    hLayMain->addLayout(vLayOperation);
    hLayMain->addWidget(sepLine);
    // account
    hLayBound->addWidget(btnweibo);
    hLayBound->addWidget(btnweichat);
    hLayBound->addWidget(btnQQ);
    hLayBound->addWidget(btnDouban);
    hLayBound->addWidget(btnYixin);
    hLayBound->addWidget(btnBound);
    hLayBound->addStretch();
    hLayPerson->addWidget(btnPerInfo);
    hLayPerson->addWidget(btnFriend);
    hLayPerson->addStretch();
    vLayAccount->addWidget(labelAccount);
    vLayAccount->addLayout(hLayBound);
    vLayAccount->addLayout(hLayPerson);
    vLayScrollArea->addLayout(vLayAccount);
    vLayScrollArea->addWidget(lineAccount);
    // general
    vLayGeneral->addWidget(labelGeneral);
    vLayGeneral->addWidget(labelFont);
    vLayGeneral->addWidget(cBoxFont);
    vLayGeneral->addWidget(labelStart);
    vLayGeneral->addWidget(checkStart);
    vLayGeneral->addWidget(labelConnect);
    vLayGeneral->addWidget(checkConnect);
    vLayGeneral->addWidget(labelAnimate);
    vLayGeneral->addWidget(checkAnimate);
    vLayGeneral->addWidget(labelGPU);
    vLayGeneral->addWidget(checkGPU);
    vLayGeneral->addWidget(labelColse);
    vLayGeneral->addWidget(rbtnMin);
    vLayGeneral->addWidget(rbtnClose);
    vLayGeneral->addWidget(labelTimedColse);
    vLayGeneral->addWidget(checkTimedClose);
    vLayScrollArea->addLayout(vLayGeneral);
    vLayScrollArea->addWidget(lineGeneral);
    // play
    vLayPlay->addWidget(labelPlay);
    vLayPlay->addWidget(labelPlayList);
    vLayPlay->addWidget(rbtnReplaceList);
    vLayPlay->addWidget(rbtnAddList);
    vLayPlay->addWidget(labelAutoPlay);
    vLayPlay->addWidget(checkAutoPlay);
    vLayPlay->addWidget(labelProgress);
    vLayPlay->addWidget(checkProgress);
    vLayPlay->addWidget(labelEffect);
    vLayPlay->addWidget(checkEffect);
    vLayPlay->addWidget(labelDevice);
    vLayPlay->addWidget(cBoxDevice);
    vLayScrollArea->addLayout(vLayPlay);
    vLayScrollArea->addWidget(linePlay);
    // massage and privacy
    vLayMessage->addWidget(labelMessage);
    vLayMessage->addWidget(labelPrivacyMsg);
    vLayMessage->addWidget(rbtnEveryone);
    vLayMessage->addWidget(rbtnAttention);
    vLayMessage->addWidget(labelNotify);
    vLayMessage->addWidget(checkSongList);
    vLayMessage->addWidget(checkFavour);
    vLayMessage->addWidget(checkNew);
    vLayMessage->addWidget(labelState);
    vLayMessage->addWidget(checkState);
    vLayMessage->addWidget(labelRank);
    vLayMessage->addWidget(rbtnEveryoneSee);
    vLayMessage->addWidget(rbtnAttentionSee);
    vLayMessage->addWidget(rbtnOnlySelfSee);
    vLayScrollArea->addLayout(vLayMessage);

    vLayScrollArea->addStretch();
    widgetMain->setLayout(vLayScrollArea);
    scrollArea->setWidget(widgetMain);
    hLayMain->addWidget(scrollArea);
    setLayout(hLayMain);

    // connect
    connect(btnTest1, &QPushButton::clicked, this, &Widget::btnTest1_clicked);
    connect(btnTest2, &QPushButton::clicked, this, &Widget::btnTest2_clicked);
    connect(btnTest3, &QPushButton::clicked, this, &Widget::btnTest3_clicked);
    connect(btnTest4, &QPushButton::clicked, this, &Widget::btnTest4_clicked);

    // property setting
    btnweibo->setFixedWidth(40);
    btnweichat->setFixedWidth(40);
    btnQQ->setFixedWidth(40);
    btnDouban->setFixedWidth(40);
    btnYixin->setFixedWidth(40);
    btnBound->setText(QString::fromLocal8Bit("绑定账号 >"));
    btnPerInfo->setText(QString::fromLocal8Bit("修改个人信息"));
    btnFriend->setText(QString::fromLocal8Bit("寻找并邀请好友"));
    cBoxFont->setFixedWidth(150);
    cBoxDevice->setFixedWidth(250);
    cBoxFont->addItem(QString::fromLocal8Bit("微软雅黑"));
    cBoxDevice->addItem(QString::fromLocal8Bit("DirectSound 主声音驱动程序"));

    // objectname setting
    labelAccount->setObjectName("firstTitle");
    labelGeneral->setObjectName("firstTitle");
    labelPlay->setObjectName("firstTitle");
    labelMessage->setObjectName("firstTitle");
    labelFont->setObjectName("secondTitle");
    labelStart->setObjectName("secondTitle");
    labelConnect->setObjectName("secondTitle");
    labelAnimate->setObjectName("secondTitle");
    labelGPU->setObjectName("secondTitle");
    labelColse->setObjectName("secondTitle");
    labelTimedColse->setObjectName("secondTitle");
    labelPlayList->setObjectName("secondTitle");
    labelAutoPlay->setObjectName("secondTitle");
    labelProgress->setObjectName("secondTitle");
    labelEffect->setObjectName("secondTitle");
    labelDevice->setObjectName("secondTitle");
    labelPrivacyMsg->setObjectName("secondTitle");
    labelNotify->setObjectName("secondTitle");
    labelState->setObjectName("secondTitle");
    labelRank->setObjectName("secondTitle");
    // separate line setting
    sepLine->setFixedWidth(1);
    sepLine->setFrameShape(QFrame::VLine);
    lineAccount->setFixedHeight(1);
    lineAccount->setFrameShape(QFrame::HLine);
    lineGeneral->setFixedHeight(1);
    lineGeneral->setFrameShape(QFrame::HLine);
    linePlay->setFixedHeight(1);
    linePlay->setFrameShape(QFrame::HLine);
    sepLine->setObjectName("separateLine");
    lineAccount->setObjectName("separateLine");
    lineGeneral->setObjectName("separateLine");
    linePlay->setObjectName("separateLine");
    // setting
    scrollArea->setWidgetResizable(true);
    // style setting
    setStyleSheet("QWidget{font-family:Microsoft YaHei;}");
    QString firstTitleStyle = "QLabel#firstTitle{font-size:20px;"
                              "margin-top:15px;"
                              "margin-bottom:10px;}";
    labelAccount->setStyleSheet(firstTitleStyle);
    labelGeneral->setStyleSheet(firstTitleStyle);
    labelPlay->setStyleSheet(firstTitleStyle);
    labelMessage->setStyleSheet(firstTitleStyle);
    QString secondTitleStyle = "QLabel#secondTitle{font-size:12px;"
                               "margin-top:15px;"
                               "margin-bottom:3px;}";
    labelFont->setStyleSheet(secondTitleStyle);
    labelStart->setStyleSheet(secondTitleStyle);
    labelConnect->setStyleSheet(secondTitleStyle);
    labelAnimate->setStyleSheet(secondTitleStyle);
    labelGPU->setStyleSheet(secondTitleStyle);
    labelColse->setStyleSheet(secondTitleStyle);
    labelTimedColse->setStyleSheet(secondTitleStyle);
    labelPlayList->setStyleSheet(secondTitleStyle);
    labelAutoPlay->setStyleSheet(secondTitleStyle);
    labelProgress->setStyleSheet(secondTitleStyle);
    labelEffect->setStyleSheet(secondTitleStyle);
    labelDevice->setStyleSheet(secondTitleStyle);
    labelPrivacyMsg->setStyleSheet(secondTitleStyle);
    labelNotify->setStyleSheet(secondTitleStyle);
    labelState->setStyleSheet(secondTitleStyle);
    labelRank->setStyleSheet(secondTitleStyle);
    // separate line style
    QString lineStyle = "QFrame#separateLine{border:none;"
                        "background-color:#E1E1E2;}";
    sepLine->setStyleSheet(lineStyle);
    lineAccount->setStyleSheet(lineStyle);
    lineGeneral->setStyleSheet(lineStyle);
    linePlay->setStyleSheet(lineStyle);
    // scrollarea style
    QString areaStyle = "QScrollArea{border:none;}";
    scrollArea->setStyleSheet(areaStyle);
    // scrollbar style
    QString scrollbarStyle = "QScrollBar::vertical {"
                             "background-color:rgba(0, 0, 0, 0);"
                             "border: none;"
                             "width: 12px;}"
                             "QScrollBar::handle:vertical {"
                             "background: rgb(204, 204, 204);"
                             "min-width: 12px;"
                             "border-radius: 5px;}"
                             "QScrollBar::add-line:vertical {"
                             "border: none;}"
                             "QScrollBar::sub-line:vertical {"
                             "border: none;}";
    scrollArea->verticalScrollBar()->setStyleSheet(scrollbarStyle);
}

Widget::~Widget()
{
    btnTest1->deleteLater();
    btnTest2->deleteLater();
    btnTest3->deleteLater();
    btnTest4->deleteLater();
    btnTest5->deleteLater();
    btnTest6->deleteLater();
    btnTest7->deleteLater();
    btnTest8->deleteLater();
    vLayOperation->deleteLater();
    sepLine->deleteLater();
    // account
    btnweibo->deleteLater();
    btnweichat->deleteLater();
    btnQQ->deleteLater();
    btnDouban->deleteLater();
    btnYixin->deleteLater();
    btnBound->deleteLater();
    hLayBound->deleteLater();
    btnPerInfo->deleteLater();
    btnFriend->deleteLater();
    hLayPerson->deleteLater();
    vLayAccount->deleteLater();
    lineAccount->deleteLater();
    // general
    labelGeneral->deleteLater();
    labelFont->deleteLater();
    cBoxFont->deleteLater();
    labelStart->deleteLater();
    checkStart->deleteLater();
    labelConnect->deleteLater();
    checkConnect->deleteLater();
    labelAnimate->deleteLater();
    checkAnimate->deleteLater();
    labelGPU->deleteLater();
    checkGPU->deleteLater();
    labelColse->deleteLater();
    rbtnMin->deleteLater();
    rbtnClose->deleteLater();
    labelTimedColse->deleteLater();
    checkTimedClose->deleteLater();
    vLayGeneral->deleteLater();
    lineGeneral->deleteLater();
    // play
    labelPlay->deleteLater();
    labelPlayList->deleteLater();
    rbtnReplaceList->deleteLater();
    rbtnAddList->deleteLater();
    labelAutoPlay->deleteLater();
    checkAutoPlay->deleteLater();
    labelProgress->deleteLater();
    checkProgress->deleteLater();
    labelEffect->deleteLater();
    checkEffect->deleteLater();
    labelDevice->deleteLater();
    cBoxDevice->deleteLater();
    vLayPlay->deleteLater();
    linePlay->deleteLater();
    // message and privacy
    labelMessage->deleteLater();
    labelPrivacyMsg->deleteLater();
    rbtnEveryone->deleteLater();
    rbtnAttention->deleteLater();
    labelNotify->deleteLater();
    checkSongList->deleteLater();
    checkFavour->deleteLater();
    checkNew->deleteLater();
    labelState->deleteLater();
    checkState->deleteLater();
    labelRank->deleteLater();
    rbtnEveryoneSee->deleteLater();
    rbtnAttentionSee->deleteLater();
    rbtnOnlySelfSee->deleteLater();
    vLayMessage->deleteLater();

    vLayScrollArea->deleteLater();
    widgetMain->deleteLater();
    scrollArea->deleteLater();
    hLayMain->deleteLater();
    delete ui;
}

void Widget::btnTest1_clicked()
{
    QScrollBar *bar = scrollArea->verticalScrollBar();
    bar->setValue(bar->minimum());
}

void Widget::btnTest2_clicked()
{
    QScrollBar *bar = scrollArea->verticalScrollBar();
    bar->setValue(140);
}

void Widget::btnTest3_clicked()
{
    scrollArea->verticalScrollBar()->setValue(700);
}

void Widget::btnTest4_clicked()
{
    QScrollBar *bar = scrollArea->verticalScrollBar();
    bar->setValue(bar->maximum());
}

