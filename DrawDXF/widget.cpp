﻿#include "widget.h"
#include "ui_widget.h"
#include <QtDebug>
#include "jtextitem.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget),
    hLayoutXCenter(new QHBoxLayout),
    hLayoutYCenter(new QHBoxLayout),
    hLayoutXOffset(new QHBoxLayout),
    hLayoutYOffset(new QHBoxLayout),
    hLayoutZoom(new QHBoxLayout),
    vLayoutParam(new QVBoxLayout),
    hLayoutMain(new QHBoxLayout),
    XOffset(20404000),
    YOffset(3918000),
    ZoomDelta(0.1),
    XCenter(1500),
    YCenter(800)
{
    ui->setupUi(this);
    Scene = new QGraphicsScene;
    Scene->setBackgroundBrush(QBrush(QColor(0, 0, 0)));
    View = new InteractiveView(this);
    View->setInteractive(true);
    View->setDragMode(QGraphicsView::ScrollHandDrag);
    View->setScene(Scene);
    View->setRenderHints(QPainter::SmoothPixmapTransform);
    View->show();
    vLayoutParam->addWidget(ui->BtnReadDXF);
    hLayoutXCenter->addWidget(ui->label_4);
    hLayoutXCenter->addWidget(ui->EditXCenter);
    vLayoutParam->addLayout(hLayoutXCenter);
    hLayoutYCenter->addWidget(ui->label_5);
    hLayoutYCenter->addWidget(ui->EditYCenter);
    vLayoutParam->addLayout(hLayoutYCenter);
    hLayoutXOffset->addWidget(ui->label);
    hLayoutXOffset->addWidget(ui->XOffset);
    vLayoutParam->addLayout(hLayoutXOffset);
    hLayoutYOffset->addWidget(ui->label_2);
    hLayoutYOffset->addWidget(ui->YOffset);
    vLayoutParam->addLayout(hLayoutYOffset);
    hLayoutZoom->addWidget(ui->label_3);
    hLayoutZoom->addWidget(ui->Zoom);
    vLayoutParam->addLayout(hLayoutZoom);
    vLayoutParam->addWidget(ui->pushButton);
    vLayoutParam->addWidget(ui->pushButton_2);
    vLayoutParam->addStretch();
    hLayoutMain->addWidget(View);
    hLayoutMain->addLayout(vLayoutParam);
    hLayoutMain->setContentsMargins(0, 0, 5, 0);
    hLayoutMain->setStretch(0, 90);
    hLayoutMain->setStretch(1, 10);
    this->setLayout(hLayoutMain);
    ui->EditXCenter->setText(QString::number(XCenter, 'f', 3));
    ui->EditYCenter->setText(QString::number(YCenter, 'f', 3));
    ui->XOffset->setText(QString::number(XOffset, 'f', 3));
    ui->YOffset->setText(QString::number(YOffset, 'f', 3));
    ui->Zoom->setText(QString::number(ZoomDelta, 'f', 3));

    // setting
//    View->setViewportUpdateMode(QGraphicsView::NoViewportUpdate);
}

Widget::~Widget()
{
//    delete dxf;
//    dxf = nullptr;
    QList<QGraphicsItem *> items = Scene->items();
    for (auto i = 0; i < items.size(); i++) {
        Scene->removeItem(items[i]);
        delete items[i];
    }
    hLayoutXCenter->deleteLater();
    hLayoutYCenter->deleteLater();
    hLayoutXOffset->deleteLater();
    hLayoutYOffset->deleteLater();
    hLayoutZoom->deleteLater();
    vLayoutParam->deleteLater();
    hLayoutMain->deleteLater();
    Scene->deleteLater();
    View->deleteLater();
    delete ui;
}

void Widget::on_BtnReadDXF_clicked()
{
    QString file = "F:/QuakeCollectLocate/Cad/zl_ansi.dxf";
    DxfReader dxfReader(file);
    dxfText << dxfReader.dxfText;
    QPen pen;
    for(auto d: dxfReader.dxfText) {
        QString text = d.text.data();
        text.replace("%%D", QString::fromLocal8Bit("°"));
        QGraphicsTextItem *textItem = new QGraphicsTextItem(text);
        textItem->setX(d.ipx - XOffset);
        textItem->setY(YOffset - d.ipy - 13);
        textItem->setDefaultTextColor(Qt::white);
        textItem->setScale(0.5);
        textItem->setCacheMode(QGraphicsItem::ItemCoordinateCache);
        if(d.angle > 0.3) {
            int angle = static_cast<int>(-d.angle  * 57.2958);
            textItem->setRotation(angle);

        }
//        textItem.
        Scene->addItem(textItem);
    }
    pen.setColor(Qt::yellow);
    pen.setWidth(0);
    for(auto d: dxfReader.dxfLines) {
        QLineF line(d.x1 - XOffset, YOffset - d.y1, d.x2 - XOffset, YOffset - d.y2);
        dxfLines << line;
        QGraphicsLineItem *lineItem = new QGraphicsLineItem(line);
        pen.setCosmetic(true);
        lineItem->setPen(pen);
        Scene->addItem(lineItem);
    }

}

void Widget::on_XOffset_editingFinished()
{
    XOffset = ui->XOffset->text().toDouble();
}

void Widget::on_YOffset_editingFinished()
{
    YOffset = ui->YOffset->text().toDouble();
}

void Widget::on_Zoom_editingFinished()
{
    ZoomDelta = ui->Zoom->text().toDouble();
    qreal factor = View->transform().scale(static_cast<qreal>(ZoomDelta), static_cast<qreal>(ZoomDelta)).mapRect(QRectF(0, 0, 1, 1)).width();
    if (factor < 0.07 || factor > 100)
        return;
    View->scale(ZoomDelta, ZoomDelta);
}

void Widget::on_EditXCenter_editingFinished()
{
    XCenter = ui->EditXCenter->text().toDouble();
    View->centerOn(XCenter, YCenter);
}

void Widget::on_EditYCenter_editingFinished()
{
    YCenter = ui->EditYCenter->text().toDouble();
    View->centerOn(XCenter, YCenter);
}

void Widget::on_pushButton_clicked()
{
#if 0
    qreal x = 20404614;
    qreal y = 3918540;
    QGraphicsEllipseItem* Ellipse = new QGraphicsEllipseItem;
    Ellipse->setRect(x - XOffset, y - YOffset, 30, 30);
    Ellipse->setPen(QColor(Qt::white));
    Ellipse->setBrush(QBrush(QColor(Qt::red)));
    Ellipse->setFlags(QGraphicsItem::ItemIsSelectable);
    Scene->addItem(Ellipse);
    View->centerOn(x - XOffset, y - YOffset);
#endif
    /*
    JTextItem *textItem = new JTextItem("Hello World");
    textItem->setX(0);
    textItem->setY(0);
    textItem->setDefaultTextColor(Qt::white);
    textItem->setScale(0.5);
    textItem->setCacheMode(QGraphicsItem::ItemCoordinateCache);
    textItem->setFlag(QGraphicsItem::ItemClipsToShape);
    Scene->addItem(textItem);
    View->centerOn(0, 0);
    */

    QPen pen;
    pen.setWidth(0);
    pen.setColor(QColor(212, 205, 34));
    QPolygonF poly;
    poly << QPointF(10, 10) << QPointF(100, 10) << QPointF(100, 100) << QPointF(20, 20);
    for(auto d = poly.begin(); d != poly.end();) {
        auto p1 = *d++;
        if(d == poly.end())
            continue;
        auto p2 = *d;
        QGraphicsLineItem *lineItem = new QGraphicsLineItem(QLineF(p1, p2));
        pen.setCosmetic(true);
        lineItem->setPen(pen);
        Scene->addItem(lineItem);

//        QGraphicsPolygonItem *item = new QGraphicsPolygonItem(poly);
//        Scene->addItem(item);
//        item->setPen(pen);
    }
    View->centerOn(0, 0);
}

void Widget::on_pushButton_2_clicked()
{
#if 1
    qreal x = 20404614;
    qreal y = 3918540;
    // x - XOffset - 30, y - YOffset - 30, x - XOffset + 30, y - YOffset + 30
    QRectF Rect(x - XOffset, y - YOffset, 5 , 5);
    QList<QGraphicsItem *> itemList = Scene->items(Rect);
#else
    QList<QGraphicsItem *> itemList = Scene->selectedItems();
#endif
    for (auto i = 0; i < itemList.size(); i++) {
        Scene->removeItem(itemList[i]);
        delete itemList[i];
    }
    qDebug() << qVersion();
}
